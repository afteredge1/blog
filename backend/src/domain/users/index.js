const UserModel = require('../../models/User');

const getUsers = async () => {
    const userList = await UserModel.find({});
    return userList;
}

const createUser = async (user) => {
    const newUser = await UserModel.create(user);
    return newUser;
}

module.exports = {
    getUsers,
    createUser
};
