const mongoose = require('mongoose');

const BlogSchema = new mongoose.Schema(
    {
        title: {
            type: String,
            required: true
        },
        slug: {
            type: String,
            required: true,
            unique: true
        },
        image: {
            type: String,
            required: true
        },
        description: {
            type: String,
            default: null
        },
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true
        }
    },
    { timestamps: true, strict: true }
);

const BlogModel = mongoose.model('blog', BlogSchema);

module.exports = BlogModel;
