const mongoose = require('mongoose');

const UserModel = require('./User');
const BlogModel = require('./Blog');

const connectDb = () => {
    console.log('connecting to mongodb');
    return mongoose
        .connect('mongodb://mongodb:27017/blog')
        .then(() => console.log('successfully connected with mongodb'))
        .catch((err) => console.log('failed to connect', err));
}

module.exports = {
    connectDb,
    UserModel,
    BlogModel
};
