const userDomain = require('../../../domain/users');

module.exports = (app) => {
    // get
    app.get('/', async (req, res) => {
        try {
            const users = await userDomain.getUsers();
            res.status(200).json(users);
        } catch (err) {
            res.status(500).json({ message: err.message })
        }
    })
    
    // post
    app.post('/', async (req, res) => {
        try {
            const user = req.body;
            const newUser = await userDomain.createUser(user);
            res.status(200).json({ message: "user created", user: newUser });
        } catch (error) {
            console.error("Failed to create mappings", { error: error });
            res.status(500).json({ error: error.message });
        }
    })

    return app;
}
