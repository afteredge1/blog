const express = require('express');
const router = express.Router();

const usersApi = require('./users');
const blogsApi = require('./blogs');

router.use('/users', usersApi(express.Router()));
router.use('/blogs', express.Router(blogsApi));


module.exports = router;
