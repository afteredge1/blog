const express = require('express');
const app = express();
const port = 3000;

app.use(express.json());

const router = require('./src/api/http');

app.use('/api', router);

const { connectDb } = require('./src/models');

const startApp = app.listen(port, () => {
  console.log(`server is listening on port ${port}`)
})

Promise.all([connectDb()])
  .then(() => startApp)
  .catch((error) => console.error("failed to start service", error));
